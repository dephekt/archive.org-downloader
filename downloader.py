#!/usr/bin/env python3
import argparse
import os
import logging
import urllib.parse
import zipfile

import requests
import bs4
import py7zr
from tqdm import tqdm

SUPPORTED_EXTENSIONS = ["7z", "zip"]


logging.basicConfig(level=logging.INFO,
                    format='%(message)s',
                    handlers=[logging.StreamHandler(),
                              logging.FileHandler('download.log')])

logger = logging.getLogger()


def download_files(url, output_dir, extract_extensions):
    # Check if the entered extensions are supported
    for ext in extract_extensions:
        if ext not in SUPPORTED_EXTENSIONS:
            logger.error(
                f"Error: Extension '{ext}' not supported. Supported extensions: {', '.join(SUPPORTED_EXTENSIONS)}")
            return

    # Fetch the webpage
    response = requests.get(url)
    if not response.ok:
        logger.error(f"Failed to fetch the webpage. Status code: {response.status_code}")
        return

    # Parse the webpage
    soup = bs4.BeautifulSoup(response.text, 'html.parser')
    table = soup.find('table', {'class': 'directory-listing-table'})

    # Extract name for output directory from the URL
    # This will be the last thing in the URL after the last slash
    # e.g. https://example.com/path/to/directory -> directory
    subdirectory_name = url.rstrip('/').split('/')[-1]

    # Then reconstruct the output path, adding the subdirectory name
    output_dir = os.path.join(output_dir, subdirectory_name)

    # Create output directory if not exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Download the files
    for row in table.find('tbody').find_all('tr'):
        # Skip the "parent directory" link at the top of the table
        if row.find('span', {'title': 'Parent Directory'}):
            continue

        link = row.find('a')['href']

        # Skip directories
        if link.endswith('/'):
            continue

        file_url = url.rstrip('/') + '/' + link.lstrip('/')
        logger.debug(f"Final download URL for file: {file_url}")

        file_name = urllib.parse.unquote(link.split('/')[-1])
        file_path = os.path.join(output_dir, file_name)
        logger.info(f"Downloading {file_name}...")

        response = requests.get(file_url, stream=True)
        total_size = int(response.headers.get('content-length', 0))

        # Bail if we already have the file and the size matches
        if os.path.getsize(file_path) == total_size:
            logger.info(f"{file_name} already downloaded.")
            continue

        progress_bar = tqdm(total=total_size, unit='iB', unit_scale=True)
        with open(file_path, 'wb') as f:
            for chunk in response.iter_content(chunk_size=8192):
                progress_bar.update(len(chunk))
                f.write(chunk)
        progress_bar.close()
        logger.info(f"{file_name} downloaded.")

        # Extract the file if its extension is in extract_extensions
        if os.path.splitext(file_name)[1][1:] in extract_extensions:
            extract_file(file_path, output_dir)


def extract_file(file_path, output_dir):
    file_name = os.path.basename(file_path)
    file_extension = os.path.splitext(file_name)[1][1:]
    extract_dir = os.path.join(output_dir, os.path.splitext(file_name)[0])
    if file_extension in SUPPORTED_EXTENSIONS:
        if file_extension == '7z':
            with py7zr.SevenZipFile(file_path, 'r') as archive:
                archive.extractall(extract_dir)
            logger.info(f"{file_name} extracted to {extract_dir}.")
        elif file_extension == 'zip':
            with zipfile.ZipFile(file_path, 'r') as archive:
                archive.extractall(extract_dir)
            logger.info(f"{file_name} extracted to {extract_dir}.")
    else:
        logger.error(f"Error: Extension '{file_extension}' is not supported. Supported extensions are: {', '.join(SUPPORTED_EXTENSIONS)}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="URL to download to crawl")
    parser.add_argument("-d", "--dir", help="Output directory")
    parser.add_argument(
        "-e", "--extract-extensions", action="append",
        help="Try to extract files matching the extension. Supported extensions: 7z"
    )
    args = parser.parse_args()
    if args.extract_extensions:
        download_files(args.url, args.dir, args.extract_extensions)
    else:
        download_files(args.url, args.dir, [])
